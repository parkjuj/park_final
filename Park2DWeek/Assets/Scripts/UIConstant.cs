﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIConstant : MonoBehaviour
{
    public TextMeshProUGUI scoreText;

    public int count;
    public static UIConstant constant;

    private void Start()
    {
        if (!constant)
        {
            constant = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
