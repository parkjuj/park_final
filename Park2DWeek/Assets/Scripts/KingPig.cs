﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingPig : MonoBehaviour
{

    Rigidbody2D rB2D;

    public float speed;
    public float distance;
    public Animator animator;

    public SpriteRenderer spriteRenderer;

    private bool movingRight = true;

    public Transform groundDetection;

    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        InvokeRepeating("flipdirection", 4f, 4f);
    }

    private void Update()
    {

        {
            if (movingRight == true)
            {
                rB2D.velocity = new Vector2(1f * speed * Time.deltaTime, rB2D.velocity.y);
                transform.eulerAngles = new Vector3(0, -180, 0);
                spriteRenderer.flipX = false;
                animator.SetBool("KingPigRun", true);
            }
            else
            {
                rB2D.velocity = new Vector2(-1f * speed * Time.deltaTime, rB2D.velocity.y);
                transform.eulerAngles = new Vector3(0, 0, 0);
                spriteRenderer.flipX = false;
                animator.SetBool("KingPigRun", false);
            }
        }
    }

    void flipdirection()
    {
        movingRight = !movingRight;
    }
}