﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rB2D;

    public float runSpeed;

    public Animator animator;

    public SpriteRenderer spriteRenderer;

    public float jumpForce;
    public int count;



    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();


        SetCountText();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }

    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0)
            spriteRenderer.flipX = false;
        else if (rB2D.velocity.x < 0)
            spriteRenderer.flipX = true;

        if (Mathf.Abs(horizontalInput) > 0f)
            animator.SetBool("IsRunning", true);
        else
            animator.SetBool("IsRunning", false);
    }

    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            Destroy(other.gameObject);
            UIConstant.constant.count = UIConstant.constant.count + 10;
            SetCountText();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Destroy(other.gameObject);
            UIConstant.constant.count = UIConstant.constant.count + 100;
            SetCountText();
        }
        else if (other.gameObject.CompareTag("King"))
        {
            SceneManager.LoadScene(5);
        }
    }
    void SetCountText()
    {
        UIConstant.constant.scoreText.text = "TOTAL SCORE: " + UIConstant.constant.count.ToString();
    }
}
