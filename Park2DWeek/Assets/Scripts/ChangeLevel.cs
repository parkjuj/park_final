﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeLevel : MonoBehaviour
{
    public string newLevel;
    void OnTriggerEnter2D(Collider2D other) //Other in this case is doors. 
    {
        if (other.CompareTag("Player"))
        {
            SceneManager.LoadScene(newLevel); //newLevel set as a part of the script component, changes to next level. 
        }
    }
    public void Restart()
    {

        SceneManager.LoadScene(1);
    }


}
