﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{

    Rigidbody2D rB2D;

    public float speed;
    public float distance;
    public Animator animator;

    public SpriteRenderer spriteRenderer;

    private bool movingRight = true;

    public Transform groundDetection;

    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        InvokeRepeating("flipdirection", 3f, 3f);
    }

    private void Update()
    {
        //transform.Translate(Vector2.right * speed * Time.deltaTime);

        //RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);
       // if (groundInfo.collider == false)
        {
            if (movingRight == true)
            {
                rB2D.velocity = new Vector2(1f * speed * Time.deltaTime, rB2D.velocity.y);
                transform.eulerAngles = new Vector3(0, -180, 0);
                spriteRenderer.flipX = false;
                animator.SetBool("PigRun", true);
            }
            else 
            {
                rB2D.velocity = new Vector2(-1f * speed * Time.deltaTime, rB2D.velocity.y);
                transform.eulerAngles = new Vector3(0, 0, 0);
                spriteRenderer.flipX = false;
                animator.SetBool("PigRun", false);
            }
        }
    }

    void flipdirection()
    {
        movingRight = !movingRight;
    }
}
